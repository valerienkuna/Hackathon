var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var passport = require('passport')
  , FacebookStrategy = require('passport-facebook').Strategy;

 
var hbs = require('hbs');
var app = express();
 
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.use(bodyParser());

app.get('/', function(req, res) {
    res.render('index', {title:'hackathon'});

});
app.get('/auth/facebook', passport.authenticate('facebook'));

app.get('/auth/facebook/callback',
  passport.authenticate('facebook', { successRedirect: '/',
                                      failureRedirect: '/login' }));

passport.use(new FacebookStrategy({
    clientID: '1089125077821541',
    clientSecret: '2c43409d128b24007c11ad8291663c8e',
    callbackURL: "http://localhost:3000/auth/facebook/callback"
  },
  function(accessToken, refreshToken, profile, done) {
    // User.findOrCreate(..., function(err, user) {
    //   if (err) { return done(err); }
    //   done(null, user);
    // });
  }
));

app.listen(3000);